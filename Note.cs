﻿using System;

namespace NotebookConsole
{
    public class Note
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Phone_number { get; set; }
        public string Country { get; set; }
        public DateTime Birthday { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Other { get; set; }
        public Note()
        {
        }
    }
}

