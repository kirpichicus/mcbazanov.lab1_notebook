﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookConsole
{
    class Program
    {
        public static Dictionary<int, Note> database = new Dictionary<int, Note>();
        
        static void Main(string[] args)
        {
            bool exit = true;
            while (exit)
            {
                Console.Clear();
                ShowMenu();
                char c = Console.ReadKey().KeyChar;
                switch (c)
                {
                    case '1':{AddNewNote();break;}
                    case '2':{СhangeNote();break;}
                    case '3':{DeleteNote();break;}
                    case '4':{ShowNote();break;}
                    case '5':{ShowAll();break;}
                    case '6':
                        {
                            while(true)
                            {
                                Console.Clear();
                                Console.WriteLine("Вы точно хотите выйти?(да/нет)");
                                string s = Console.ReadLine();
                                if (s.ToLower() == "да")
                                {
                                    exit = false;
                                    break;
                                }
                                else
                                {
                                    if (s.ToLower() == "нет") break;
                                    else continue;
                                }
                            }
                            break;
                        }
                    default:continue;
                }
                Console.Clear();
            }
            Console.WriteLine("Досвидос!!! С вами было приятно иметь дело:)");
            Console.ReadKey();
        }//основная функция
        static void ShowMenu()
        {
            Console.WriteLine("Записная книжка");
            Console.WriteLine("---------------------------");
            Console.WriteLine("1. Создание учетной записи");
            Console.WriteLine("2. Редактирование учетной записи");
            Console.WriteLine("3. Удаление учетной записи");
            Console.WriteLine("4. Просмотр учетной записи");
            Console.WriteLine("5. Просмотр всех учетных записей");
            Console.WriteLine("6. Выход из записной книжки");
        }//готово
        static void AddNewNote()
        {
            Console.Clear();
            string s;
            int id = Note.totalNotes;
            Note temp = new Note();
            while (true)
            {
                Console.Write("Имя - ");
                s = Console.ReadLine();
                if (s != "")
                {
                    temp.Name = s;
                    break;
                }
                Console.WriteLine("Имя не может быть пустым");
            }//Пишем Имя
            while (true)
            {
                Console.Write("Фамилия - ");
                s = Console.ReadLine();
                if (s != "")
                {
                    temp.Surname = s;
                    break;
                }
                Console.WriteLine("Фамилия не может быть пустой");
            }//Пишем Фамилию
            Console.Write("Отчество - ");
            s = Console.ReadLine();
            temp.Patronymic = s;
            while (true)
            {
                Console.Write("Номер телефона - ");
                s = Console.ReadLine();
                try
                {
                    long.Parse(s);
                }
                catch(FormatException)
                {
                    Console.WriteLine("Некорректный номер");
                    continue;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Некорректный номер");
                    continue;
                }
                if (long.Parse(s) < 0)
                {
                    Console.WriteLine("Некорректный номер");
                    continue;
                }
                if (s.Length < 5)
                {
                    Console.WriteLine("Номер слишком короткий");
                    continue;
                }
                temp.Phone_number = s;
                break;
            }//Вводим номер
            while (true)
            {
                Console.Write("Страна - ");
                s = Console.ReadLine();
                if (s != "")
                {
                    temp.Country = s;
                    break;
                }
                Console.WriteLine("Страна не может быть пустой");
            }//Указываем страну
            while (true)
            {
                Console.Write("Дата рождения - ");
                //DateTime t=DateTime.MinValue;
                try
                {
                    //t = DateTime.Parse(Console.ReadLine());
                    s = Console.ReadLine();
                    if (s=="")
                    {
                        temp.Birthday = DateTime.MaxValue;
                        break;
                    }
                    DateTime.Parse(s);
                }
                catch(FormatException)
                {
                    Console.WriteLine("Некорректная дата");
                    Console.WriteLine("Хотите продолжить вводить дату рождения (да/нет)? - ");
                    s = Console.ReadLine();
                    if (s.ToLower() == "нет")
                    {
                        temp.Birthday = DateTime.MaxValue;
                        break;
                    }
                    else
                    {
                        if (s.ToLower() == "да") continue;
                        else
                        {
                            Console.WriteLine("Ответ не корректен, попробуйте ввести дату еще раз");
                            continue;
                        }
                    }
                }
                temp.Birthday = DateTime.Parse(s);
                break;
            }//Вводим дату рождения
            Console.Write("Организация - ");
            s = Console.ReadLine();
            temp.Company = s;
            Console.Write("Должность - ");
            s = Console.ReadLine();
            temp.Position = s;
            Console.Write("Прочие заметки - ");
            s = Console.ReadLine();
            temp.Other = s;
            database.Add(temp.id, temp);
            Console.WriteLine("---Запись добавлена---");
            Console.ReadKey();
        }//готово
        static void ShowAll()
        {
            while (true)
            {
                Console.Clear();
                if (database.Count == 0)
                {
                    Console.WriteLine("База данных пуста");
                    Console.ReadKey();
                    return;
                }
            
                int i = 1;
                foreach (var item in database)
                {
                    Console.WriteLine(i+ ". {0} {1} {2} ",item.Value.Surname,item.Value.Name,item.Value.Phone_number);
                    i++;
                }
                Console.WriteLine("Введите \"e\" чтобы выйти");
                string s = Console.ReadLine(); ;
                if (s == "e"|| s=="у") break;
            }
            //Console.ReadKey();
        }//готово
        static void ShowNote()
        {
            while (true)
            {
                Console.Clear();
                if (database.Count == 0)
                {
                    Console.WriteLine("База данных пуста");
                    Console.ReadKey();
                    return;
                }
                int i = 1;
                foreach (var item in database)
                {
                    Console.WriteLine(i + ". {0} {1} {2} ", item.Value.Surname, item.Value.Name, item.Value.Phone_number);
                    i++;
                }//Вывод базы
                Console.WriteLine("Введите \"e\" чтобы выйти");
                Console.Write("Посмотреть учетную запись №");
                string s = Console.ReadLine();
                int c;
                if (s == "e"||s=="у") break;
                try
                {
                    c = int.Parse(s);
                }
                catch(FormatException)
                {
                    continue;
                }
                if (c > database.Count || c < 1)
                {
                    Console.WriteLine("Нет такой записи");
                    continue;
                }
                Note h = null;
                i = 1;
                foreach (var item in database)
                {
                    if (i == c) { h = item.Value; break; }
                    i++;
                }
                /*if (h == null)
                {
                    Console.WriteLine("Нет такой записи");
                    Console.ReadKey();
                    break;
                }*/
                Console.WriteLine(h);
                Console.ReadKey();
            }
        }//вроде сделано
        static void DeleteNote()
        {
            while(true)
            {
                Console.Clear();
                if (database.Count == 0)
                {
                    Console.WriteLine("База данных пуста");
                    Console.ReadKey();
                    return;
                }
                int i = 1;
                foreach (var item in database)
                {
                    Console.WriteLine(i + ". {0} {1} {2} ", item.Value.Surname, item.Value.Name, item.Value.Phone_number);
                    i++;
                }//Вывод базы
                Console.WriteLine("Введите \"e\" чтобы выйти");
                Console.Write("Удалить учетную запись №");
                string s = Console.ReadLine();
                int c;
                if (s == "e" || s == "у") break;
                try
                {
                    c = int.Parse(s);
                }
                catch (FormatException)
                {
                    continue;
                }
                if (c > database.Count || c < 1)
                {
                    Console.WriteLine("Нет такой записи");
                    continue;
                }
                i = 1;
                foreach (var item in database)//сделать потом через for
                {
                    if (i == c) {database.Remove(item.Key);break;}
                    i++;
                }
            }
        }//вроде сделано
        static void СhangeNote()
        {
            while (true)
            {
                Console.Clear();
                if (database.Count == 0)
                {
                    Console.WriteLine("База данных пуста");
                    Console.ReadKey();
                    return;
                }
                int i = 1;
                foreach (var item in database)
                {
                    Console.WriteLine(i + ". {0} {1} {2} ", item.Value.Surname, item.Value.Name, item.Value.Phone_number);
                    i++;
                }
                Console.WriteLine("Введите \"e\" чтобы выйти");
                Console.Write("Изменить учетную запись №");
                string s = Console.ReadLine();
                int c;
                if (s == "e" || s == "у") break;
                try
                {
                    c = int.Parse(s);
                }
                catch (FormatException)
                {
                    continue;
                }
                if (c > database.Count || c < 1)
                {
                    Console.WriteLine("Нет такой записи");
                    continue;
                }
                i = 1;
                foreach (var item in database)//сделать потом через for
                {
                    if (i == c)
                    {
                        Console.Write("Имя - " + item.Value.Name + "\nНовое -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Name = s;
                        Console.Write("Фамилия - " + item.Value.Surname + "\nНовая -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Surname = s;
                        Console.Write("Отчество - " + item.Value.Patronymic + "\nНовое -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Patronymic = s;
                        for (int j = 0; j < 1; j++)
                        {
                            Console.Write("Номер телефона - " + item.Value.Phone_number + "\nНовый -");
                            s = Console.ReadLine();
                            if (s != "")
                            {
                                try
                                {
                                    long.Parse(s);
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Введенный номер некорректен. Изменения не будут приняты");
                                    break;
                                }
                                catch (OverflowException)
                                {
                                    Console.WriteLine("Введенный номер некорректен. Изменения не будут приняты");
                                    break;
                                }
                                if (long.Parse(s) < 0)
                                {
                                    Console.WriteLine("Введенный номер некорректен. Изменения не будут приняты");
                                    break;
                                }
                                if (s.Length < 5)
                                {
                                    Console.WriteLine("Введенный номер некорректен. Изменения не будут приняты");
                                    break;
                                }
                                item.Value.Phone_number = s;
                            }
                        }//Изменение телефонного номера
                        Console.Write("Страна - " + item.Value.Country + "\nНовая -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Country = s;
                        for (int j = 0; j < 1; j++)
                        {
                            Console.Write("Дата рождения - " + item.Value.Birthday + "\nНовая -");
                            try
                            {
                                s = Console.ReadLine();
                                if (s == "") break;
                                DateTime.Parse(Console.ReadLine());
                            }
                            catch(FormatException)
                            {
                                Console.WriteLine("Введенная дата некорректена. Изменения не будут приняты");
                                break;
                            }
                            item.Value.Birthday = DateTime.Parse(s);
                        }//Изменение даты рождения
                        Console.Write("Организация - " + item.Value.Company + "\nНовая -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Company = s;
                        Console.Write("Должность - " + item.Value.Position + "\nНовая -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Position = s;
                        Console.Write("Прочие заметки - " + item.Value.Other + "\nНовые -");
                        s = Console.ReadLine();
                        if (s != "") item.Value.Other = s;
                        Console.WriteLine();
                        //i = 1;
                        break;
                    }
                    i++;
                }
            }
               
        }//Вроде сделано
    }
}
