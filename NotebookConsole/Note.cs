﻿using System;

namespace NotebookConsole
{
    public class Note
    {
        public static int totalNotes;
        public int id;
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Phone_number { get; set; }
        public string Country { get; set; }
        public DateTime Birthday { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Other { get; set; }
        public override string ToString()
        {
            return "Фамилия: "+ Surname+ "\nИмя: "+ Name+"\nОтчество: "+Patronymic+ "\nНомер телефона: " + Phone_number + "\nСтрана: "+Country+"\nДата рождения:"+((this.Birthday==DateTime.MaxValue)? "":this.Birthday.ToString())+"\nОрганизация: "+Company+"\nДолжность: "+Position+ "\nПримечания: " + Other ;
        }
        public Note()
        {
            this.id = totalNotes;
            totalNotes++;
        }
    }
}
